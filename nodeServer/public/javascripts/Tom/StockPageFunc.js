function loadResult(val, sourceList) {
    const sendData = {
        term: val,
        sources: sourceList
    };
    $.ajax({
        type: "POST",
        url: "http://localhost:3000/stockPage/stockSearch",
        data: JSON.stringify(sendData),
        contentType: 'application/json',
        success: function(data) {
            console.log('success');
            appendData(data);
        }
    });

    function appendData(data) {
        let words = data.words;
        let stocks = data.stocks;
        words = words.sort(generalCompareWithAbs('count'));
        stocks = stocks.sort(generalCompareWithAbs('pChange'));
        let stockBlock = $('#stockBlock');
        if (words.length === 0 || stocks.length === 0){
            $(stockBlock).append("<h3>No Results!</h3>");
            return
        }


        getAverages(data);

        let toAppend = "<div class='container border row'>" +
            "<div class='col' id='colThing'>" +
            "<h3 style='float: left;'>Date</h3>" +
            "<h3 style='float: right;'>Average Number of Occurrences</h3><br><br>" +
            "</div>" +
            "</div>";
        $(stockBlock).append(toAppend);


        let dateSet = [];
        words.forEach(function (e) {
            if (!findUnique(dateSet, e.date, 'date')) {
                dateSet.push({date: e.date, avgTOTAL: e.count, totalSRC: 1});
            } else {
                let temp = dateSet.find((v) => {
                    return e.date === v.date;
                });
                temp.avgTOTAL += e.count;
                temp.totalSRC += 1;
            }
        });



        dateSet.forEach(function (ele) {

            ele['FINAL'] = ele.avgTOTAL / ele.totalSRC;
            $('#colThing').append("<div style='cursor: pointer;' class='border center-text dateItem'>" +
                "<div style='float: left;'>" +
                "<h5>" + new Date(ele.date).toDateString() + "</h5>" +
                "</div>" +
                "<div style='float: right;'>" +
                "<h5>"+ele.FINAL+"</h5>" +
                "</div><br><br>" +
                "<div style='display: none' class='stockTableContainer'>" +
                createStockTable(ele.date) +
                "</div>" +
                "</div>");
        });

        console.log(dateSet);

        function createStockTable(date) {
            let stringThing = "<table  class='table'> <tr>" +
                "<th>Stock</th>" +
                "<th>Percent Change</th>" +
                "</tr>";
            let total = 0;
            stocks.forEach(function (s) {
                if (total < 10) {
                    if (s.date === date){
                        stringThing = stringThing.concat("<tr>" +
                            "<td>" +
                            s.stock +
                            "</td>" +
                            "<td>" +
                            s.pChange + '%' +
                            "</td>" +
                            "</tr>");
                        total++;
                    }
                }
            });
            stringThing = stringThing.concat("</table>");
            return stringThing;
        }


        $('.dateItem').on('click', function () {
            let temp = $(this).find('.stockTableContainer');
            $(temp).slideToggle();
        });

    }



    function findUnique(arr, val, prop) {
        let final = false;
        arr.forEach(function (e) {
            // console.log('STRANG: ' + e.stock + '?' + val + ': '.concat(e.stock === val));
            if (e[prop] === val) {
                final = true;
            }
        });
        return final
    }

    function generalCompareWithAbs(thingToCompare, sortBy=1) {
        return (a,b) => {
            if (Math.abs(a[thingToCompare]) > Math.abs(b[thingToCompare])) {
                return sortBy * -1
            } if (Math.abs(a[thingToCompare]) < Math.abs(b[thingToCompare])) {
                return sortBy
            }
            return 0;
        };
    }


    function getAverages(inData) {
        $.ajax({
            type: "POST",
            url: "http://localhost:3000/FlaskAnalyzer",
            data: JSON.stringify(inData),
            contentType: 'application/json',
            success: function(data) {
                console.log('success');
                displayAverages(JSON.parse(data))
            }
        });
    }

    function displayAverages(data) {
        data = data.sort(generalCompareWithAbs('weighted_avg'));
        console.log(data);
        let avgBlock = $('#averageBlock');

        $(avgBlock).append("<div style='cursor: pointer;' class='border' id='avgTableHeader'>\n" +
            "<h3>Table of Averages</h3>" +
            "</div>" +
            "<div id='avgSlideBlock' style='display: none' >" +
            "<table id='avgTable' class='table table-striped'>" +
            "<caption>Average change of stock per how much it appeared in a particular source over multiple days</caption>" +
            "<tr>" +
            "<th>" +
            "Stock" +
            "</th>" +
            "<th>" +
            "Average Change of the Stock" +
            "</th>" +
            "<th>" +
            "Source" +
            "</th>" +
            "</tr>" +
            "</table>" +
            "</div>");

        let avgTable = $('#avgTable');
        data.some((val, index) => {
            if (index > 10) return;
            let avg = val['weighted_avg'];
            let stock = val['stock'];
            let src = val['source'];

            $(avgTable).append("<tr>" +
                "<td>" +
                stock +
                "</td>" +
                "<td>" +
                avg +
                "</td>" +
                "<td>" +
                src +
                "</td>" +
                "</tr>");
        });

        $('#avgTableHeader').on('click', function () {
            $('#avgSlideBlock').slideToggle();
        });


    }
}