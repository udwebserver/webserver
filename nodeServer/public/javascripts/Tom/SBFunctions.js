function myFunction() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("SearchCourses");
    filter = input.value.toUpperCase();
    ul = document.getElementById("CourseList");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";

        }
    }
}

function LoadFunction() {
    coursesInBlock = JSON.parse(localStorage.getItem('courseList') || '[]');
    var targetID, CourseName, CRN;
    coursesInBlock.forEach(function (t) {
        targetID = t.Time;
        CourseName = t.CourseName;
        CRN = t.CRN;
        document.getElementById(targetID).appendChild(document.getElementById(CourseName).cloneNode(true));
        draggedObjectid = CourseName;
        document.getElementById(targetID).childNodes[0].childNodes[1].childNodes[3].childNodes[3].value = CRN;
        //isInPlanner = targetID.parentNode.parentNode.parentNode.parentNode.id;
        updateCH();
    });

}

function moveBack(ev) {
    var clickedObj;
    var label;
    var CHValue;
    if (ev.target.parentNode.parentNode.className === "TimeSlotList") {
        clickedObj = ev.target.parentNode;
        label = document.getElementById("CreditHours");
        CHValue = ev.target.childNodes[3].textContent.replace("CH: ", "");
        label.innerText = parseFloat(label.textContent) - parseFloat(CHValue);
        updateLocalRemove(clickedObj, ev.target.parentNode.parentNode);
        clickedObj.parentNode.removeChild(clickedObj);
        //document.getElementById("CourseList").appendChild(document.getElementById(clickedObj));
    } else if (ev.target.parentNode.parentNode.parentNode.className === "TimeSlotList"){
        clickedObj = ev.target.parentNode.parentNode;
        label = document.getElementById("CreditHours");
        CHValue = ev.target.parentNode.childNodes[3].textContent.replace("CH: ", "");
        label.innerText = parseFloat(label.textContent) - parseFloat(CHValue);
        updateLocalRemove(clickedObj, ev.target.parentNode.parentNode.parentNode);
        clickedObj.parentNode.removeChild(clickedObj);

        //document.getElementById("CourseList").appendChild(document.getElementById(clickedObj));
    }
}

var draggedObjectid;
var isInPlanner;
var coursesInBlock = [];
var courseList = JSON.parse(localStorage.getItem('courseList') || '[]');

function updateCRN(ev) {
    console.log(coursesInBlock);
    var val = ev.target.value;
    var courseName = ev.target.parentNode.parentNode.parentNode;
    var time = courseName.parentNode;
    var isThere = false;
    var i;
    for (i = 0; i < coursesInBlock.length; ++i){
        if (coursesInBlock[i].CourseName === courseName.id && coursesInBlock[i].Time === time.id) {
            isThere = true;
            break;
        }
    }
    if(isThere) {
        coursesInBlock[i].CRN = val;
        localStorage.setItem('courseList', JSON.stringify(coursesInBlock));
    }

}

function updateLocalAdd(list) {
    coursesInBlock.push({
        CourseName:draggedObjectid,
        Time:list.id,
        CRN:""
    });

    localStorage.setItem('courseList', JSON.stringify(coursesInBlock));
}

function updateLocalRemove(item, list) {
    var i = 0;
    var isThere = false;
    for (i = 0; i < coursesInBlock.length; ++i) {
        if (coursesInBlock[i].CourseName === item.id && coursesInBlock[i].Time === list.id) {
            isThere = true;
            break;
        }
    }
    if (isThere){
        coursesInBlock.splice(i, 1);
        localStorage.setItem('courseList', JSON.stringify(coursesInBlock));
    }

}


function allowDrop(ev) {
    if (ev.target.className === "TimeSlotList")
        ev.preventDefault();
    else if (ev.target.className === "ListItemA") {
        ev.preventDefault();
    }
}



function dragStart(ev) {
    isInPlanner = ev.target.parentNode.parentNode.parentNode.parentNode.id;
    draggedObjectid = ev.target.parentNode.id;
}

function onDrop(ev) {
    if (ev.target.className === "TimeSlotList") {
        ev.target.appendChild(document.getElementById(draggedObjectid).cloneNode(true));
        //if (isInPlanner === "SearchBlock")
        updateCH();
        updateLocalAdd(ev.target);
    } else  if (ev.target.className === "ListItemA") {
        ev.target.parentNode.parentNode.appendChild(document.getElementById(draggedObjectid).cloneNode(true));
        //if (isInPlanner === "SearchBlock")
        updateCH();
        updateLocalAdd(ev.target.parentNode.parentNode);
    }
}

function updateCH() {
    var label = document.getElementById("CreditHours");
    var CHValue = document.getElementById(draggedObjectid).getElementsByTagName("span")[0].textContent.replace("CH: ", "");
    label.innerText = parseFloat(label.textContent) + parseFloat(CHValue);
}


