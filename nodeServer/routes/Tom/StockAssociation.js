const express = require('express');
const router = express.Router();
const mysql = require('mysql');

const connection = mysql.createConnection({
    host:'localhost',
    user: 'phpaccess',
    password: 'phpthings',
    database: 'marketeer'
});
connection.connect();



/* GET home page. */
router.get('/',function(req, res, next) {
    res.render('Tom/StockAssociation');
});


router.post('/stockSearch', function (req, res, next) {
    console.log(req.body.term);
    let search = req.body.term;
    let srcList = req.body.sources;
    let sql = "Select * from news_table where word=?";
    let inserts = [search];
    if (srcList[0] !== "*") {
        console.log(srcList);
        sql = sql.concat(" and ( source=? ");

        srcList.forEach(function (value) {
            console.log('VALUe = '+ value);
            sql = sql.concat(" or source=?");
            inserts.push(value)
        });
        sql = sql.slice(0, sql.length - 12);
        sql = sql.concat(" )");
        console.log(sql)
    }




    sql = mysql.format(sql, inserts);
    connection.query(sql, function (error, results, fields) {
        if (error) throw error;
        let dateArr = [];
        results.forEach(function (ele) {
            dateArr.push({
                date: ele.date,
                count: ele.word_count,
                source: ele.source
            });

        });

        nextQuery(dateArr);
    });


    function nextQuery(dateArr) {
        let stockArr = [];
        let i = 0;
        let len = 1;
        sql = "select * from stock_table where ";
        inserts = [];
        dateArr.forEach(function (ele) {
            let thing = (new Date(ele.date)).toISOString().substring(0, 19).replace('T', ' ').replace('05:00:00','00:00:00');
            if (inserts.includes(thing)) { return; }
            sql = sql.concat('date=? or ');
            inserts.push(thing);
        });

        sql = sql.slice(0, -3);

        if (sql === "select * from stock_table whe") {
            res.send({stocks: stockArr, words: dateArr});
        } else {

            sql = mysql.format(sql, inserts);
            console.log(sql);

            connection.query(sql, function (err, res, fields) {
                if (err) throw err;
                len = res.length;
                res.forEach(function (ele) {
                    stockArr.push({
                        stock: ele.stock,
                        pChange: ele.percent_change,
                        date: ele.date
                    });
                });
                sendData(stockArr, dateArr);
            });
        }

        function sendData(stockArr, dateArr){
            res.send({stocks: stockArr, words: dateArr});
        }
    }
});


module.exports = router;
