var express = require('express');
var router = express.Router();
//var CourseList = require('../../server-side/data/CourseList.json');
var mysql = require('mysql');
var arr = [];
var connection = mysql.createConnection({
    host:'localhost',
    user: 'phpaccess',
    password: 'phpthings',
    database: 'classlist'
});
connection.connect();

connection.query("select * from course_table", function (error, results, fields) {
    if (error) throw error;

    results.forEach(function (ele) {
        arr.push({
           Name: ele.name,
            CH: ele.credit_hours,
            CN: ele.course_number,
            DA: ele.department
        });
    });
});


/* GET home page. */
router.get('/', function(req, res, next) {

    res.render('Tom/ScheduleBuilder', {CourseList: arr});
});


module.exports = router;