var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/CodeSwap', function(req, res, next) {
    res.render('Tom/CodeSwap');
});

module.exports = router;
