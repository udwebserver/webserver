var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/cal', function(req, res, next) {
    res.render('Schwartz/Calendar');
});

module.exports = router;