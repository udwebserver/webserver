const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const sassMiddleware = require('node-sass-middleware');
const proxy = require('http-proxy-middleware');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const Scheduler = require('./routes/Tom/ScheduleBuilder');
const Baseball = require('./routes/Tom/BaseballScoresheet');
const Calendar = require('./routes/Schwartz/Calendar');
const jswip = require('./routes/Schwartz/WIP');
const Stock = require('./routes/Tom/StockAssociation');
const CodeSwap = require('./routes/Tom/CodeSwap');
const Afen = require('./routes/Afen/AfenSite');



const app = express();

let hosturl = 'http://localhost:5000/';
if (process.env.NODE_ENV === 'production') {
    hosturl = 'http://localhost:5000/';
}

let apiProxy = proxy(hosturl);

app.use('/FlaskAnalyzer', apiProxy);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/sb', Scheduler);
app.use('/', Baseball);
app.use('/', Calendar);
app.use('/', jswip);
app.use('/stockPage', Stock);
app.use('/', CodeSwap);
app.use('/', Afen);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
