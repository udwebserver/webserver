from flask import Flask, request
import json

app = Flask(__name__)


@app.route("/FlaskAnalyzer", methods=['POST'])
def hello():
    dumps = json.dumps(request.json)
    data = json.loads(dumps)
    count_list = data['words']
    stocks = data['stocks']
    weighted_arr = []
    sum_count = 0
    final = []

    for val in count_list:
        word_date = val['date']
        count = val['count']
        source = val['source']
        sum_count += count

        for stock in stocks:
            name = stock['stock']
            change = stock['pChange']
            stock_date = stock['date']
            if change == 0:
                continue

            if stock_date == word_date:
                top_of_avg = change * count
                weighted_arr.append(dict(stock=name, top_of_avg=top_of_avg, source=source))

    for change_thing in weighted_arr:
        weighted_avg = change_thing['top_of_avg'] / sum_count
        final.append(dict(stock=change_thing['stock'], weighted_avg=weighted_avg, source=change_thing['source']))
    return json.dumps(final)
